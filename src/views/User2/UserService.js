const userService = {
  userList: [{ id: 1, name: 'Phoptham', gender: 'M' },
    { id: 2, name: 'shinobu', gender: 'M' }],
  lastid: 3,
  addUser (user) {
    user.id = this.lastid++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUser () {
    return [...this.userList]
  }
}

export default userService
